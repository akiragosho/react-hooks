import React from 'react';
import './App.scss';
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Header from './components/Header'
import ToDoList from './pages/StudyHooks/ToDoList'
import PagePostList from './pages/StudyHooks/PagePostList';
import PageMagicBox from './pages/StudyHooks/PageMagicBox';

import Products from './pages/StudyBasicReact/Products'
import BasicToDoLists from './pages/StudyBasicReact/ToDoLists'
import TrafficLight from './pages/StudyBasicReact/TrafficLight'
import ChildrenLesson from './pages/StudyBasicReact/ChildrenLesson'
import Cart from './pages/StudyBasicReact/Cart'
import DemoRef from './pages/StudyBasicReact/demo-ref'


import Home from './pages/Home';
function App() {
  return (
    <Router>
      <div className="App">
        <Header />
        <Switch>
          <Route path="/" exact component={Home} />
          <Route path="/ToDoList" exact component={ToDoList} />
          <Route path="/useEffect" exact component={PagePostList} />
          <Route path="/PageMagicBox" exact component={PageMagicBox} />
          <Route path="/products" exact component={Products} />
          <Route path="/todolist-basic" exact component={BasicToDoLists} />
          <Route path="/traffic-light" exact component={TrafficLight} />
          <Route path="/ChildrenLesson" exact component={ChildrenLesson} />
          <Route path="/Cart" exact component={Cart} />
          <Route path="/DemoRef" exact component={DemoRef} />
        </Switch>
      </div>
    </Router>
  );
}

export default App;
