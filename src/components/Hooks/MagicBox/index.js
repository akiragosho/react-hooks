import React from 'react';
import useMagicColor from '../../../useHooks/useMagicColor'
import './style.scss'
MagicBox.propTypes = {

};

function MagicBox() {
  const color = useMagicColor()
  return (
    <div>
      <div
        className="magic-box"
        style={{ backgroundColor: color }}
      >
      </div>
    </div>
  );
}

export default MagicBox;