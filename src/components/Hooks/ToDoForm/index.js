import { React, useState } from 'react';
import './style.scss'
import { Button, Input } from 'reactstrap';
import PropTypes from 'prop-types';

ToDoForm.propTypes = {
  name: PropTypes.string,
  onSubmit: PropTypes.func,
};
ToDoForm.defaultProps = {
  name: '[]',
  onSubmit: null,
};
function ToDoForm(props) {
  const { onSubmit } = props;
  const [value, setValue] = useState('')

  const handleValueChange = (e) => {
    setValue(e.target.value)
  }
  
  const handleAddList = () => {
    if (onSubmit) {
      onSubmit(value)
      setValue('')
    }
    return false
  }
  return (
    <div>
      <form className="form-add">
        <h2>Add thêm task: </h2>
        <Input type="text" placeholder="add list zô" value={value} onChange={handleValueChange} />
        <Button color="primary" onClick={handleAddList}>submit!</Button>
      </form>
    </div>
  );
}

export default ToDoForm;