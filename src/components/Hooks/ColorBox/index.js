import React, { useState } from 'react';
import PropTypes from 'prop-types';
import './style.scss'
ColorBox.propTypes = {

};

function ColorBox(){
  
  const [color, setColor] = useState(() =>{
    const initColor = localStorage.getItem('box_color') || 'blue'
    return initColor;
  });
  function getRandomColor() {
    const COLOR_LIST = ['red', 'green', 'yellow', 'black', 'blue'];
    const randomIndex = Math.floor(Math.random() * 5)
    return COLOR_LIST[randomIndex];
  }
  function changeColor(color) {
    const newColor = getRandomColor();
    setColor(newColor);
    localStorage.setItem('box_color', newColor);
  }

  return (
    <div>
      <div className="color-box" style={{ backgroundColor: color }}>
        Akira Gosho
      </div>
      <button onClick={changeColor}>bấm</button>
    </div>

  );
}

export default ColorBox;