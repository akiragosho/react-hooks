import React, { useState, useRef } from 'react';
import PropTypes from 'prop-types';
import { Form, Label, Input, Col, Row } from 'reactstrap';

import './style.scss'

PostFiltersForm.propTypes = {
  onSubmit: PropTypes.func,
};
PostFiltersForm.defaultProps = {
  onSubmit: null,
};
function PostFiltersForm(props) {
  const { onSubmit } = props;
  const [searchTerm, setSearchTerm] = useState('')
  const typingTimeoutRef = useRef(null)


  function handleSearchTermChange(e) {
    const value = e.target.value
    setSearchTerm(value);
    if (!onSubmit) return;
    if (typingTimeoutRef.current) {
      clearTimeout(typingTimeoutRef.current)
    }
    typingTimeoutRef.current = setTimeout(() => {
      const formValues = {
        searchTerm: value
      }
      onSubmit(formValues)
    }, 500)
  }


  return (
    <Form className="form-search">
      <Row>
        <Label sm={1}>Search: </Label>
        <Col sm={11}>
          <Input value={searchTerm} onChange={handleSearchTermChange} placeholder="search...." />
        </Col>
      </Row>

    </Form>
  );
}

export default PostFiltersForm;