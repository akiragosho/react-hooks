import React, { useState } from 'react';
import { BrowserRouter as  Link } from "react-router-dom";
import './style.scss';
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavbarText, UncontrolledDropdown, DropdownToggle, DropdownMenu, DropdownItem
} from 'reactstrap';

const Header = (props) => {
  const [isOpen, setIsOpen] = useState(false);
  const [dropdownOpen, setDropdownOpen] = useState(false);
  const toggle = () => setDropdownOpen(prevState => !prevState);

  // const toggle = () => setIsOpen(!isOpen);

  return (
    <div className="Header">
      <Navbar color="light" light expand="md">
        <NavbarBrand href="/">AkiraGosho's project</NavbarBrand>
        <NavbarToggler onClick={toggle} />
        <Collapse isOpen={isOpen} navbar>
          <Nav className="mr-auto" navbar>
            <NavItem>
              <Link to="/">Home</Link>
            </NavItem>
            <UncontrolledDropdown setActiveFromChild>
              <DropdownToggle tag="a" className="nav-link" caret>
                Basics
              </DropdownToggle>
              <DropdownMenu className="dropdown-menu">
                <DropdownItem tag="a" href="/todolist-basic">To Do list</DropdownItem>
                <DropdownItem tag="a" href="/traffic-light">Traffic Light</DropdownItem>
                <DropdownItem tag="a" href="/childrenLesson">childrenLesson</DropdownItem>
                <DropdownItem tag="a" href="/demoRef">Demo ref</DropdownItem>
              </DropdownMenu>
            </UncontrolledDropdown>
            <UncontrolledDropdown setActiveFromChild>
              <DropdownToggle tag="a" className="nav-link" caret>
                Hooks
              </DropdownToggle>
              <DropdownMenu className="dropdown-menu">
                <DropdownItem tag="a" href="/ToDoList">Demo useState (to do list)</DropdownItem>
                <DropdownItem tag="a" href="/useEffect">Demo useEffect (list, search)</DropdownItem>
                <DropdownItem tag="a" href="/PageMagicBox">Demo Custom Hooks</DropdownItem>
              </DropdownMenu>
            </UncontrolledDropdown>
            <NavItem>
              <Link to="/products">Products</Link>
            </NavItem>
          </Nav>
          <NavbarText className="nav-cart">
            <Link to="/Cart">Cart</Link>
          </NavbarText>
        </Collapse>
      </Navbar>
    </div>
  );
}

export default Header;