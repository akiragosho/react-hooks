import React, { Component } from "react";
import classNames from "classnames";
import PropTypes from 'prop-types'; // ES6


import './style.scss';
import check from '../../../assets/imgs/check.svg'
import unCheck from '../../../assets/imgs/uncheck.svg'
import close from '../../../assets/imgs/close.svg'

class ToDoItem extends Component {
  state = {
    isShow: false
  };
  showIcon(status) {
    if (status) {
      this.setState({
        isShow: true
      })
    } else {
      this.setState({
        isShow: false
      })
    }
  }
  render() {
    let currentImt = unCheck
    const { item, onClick, removeTask } = this.props
    if (item.status) {
      currentImt = check
    } else {
      currentImt = unCheck
    }

    const { isShow } = this.state
    return (
      <li
        onMouseEnter={() => this.showIcon(true)}
        onMouseLeave={() => this.showIcon(false)}
        className={classNames("item", {
          done: item.status,
        })}
      >
        <img onClick={onClick} src={currentImt} className="icon-check" />
        <span>{item.name}</span>
        <img onClick={removeTask} src={close} alt="" className={classNames("icon-close", {
          open: isShow
        })} />
      </li>
    );
  }
}
ToDoItem.propTypes = {
  item: PropTypes.shape({
    name: PropTypes.string,
    status: PropTypes.bool
  }),
  onClick: PropTypes.func
}
export default ToDoItem;
