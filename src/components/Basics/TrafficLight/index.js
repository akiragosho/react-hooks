import React, { Component } from "react";
import classNames from "classnames";
import "./style.scss";
const RED = 0;
const ORAGE = 1;
const GREEN = 2;
class TrafficLightComp extends Component {
  render() {
    const { currentColor } = this.props;
    return (
      <div className="traffic-light">
        <div
          className={classNames("blub", "red", {
            active: currentColor === RED,
          })}
        ></div>
        <div
          className={classNames("blub", "orange", {
            active: currentColor === ORAGE,
          })}
        ></div>
        <div
          className={classNames("blub", "green", {
            active: currentColor === GREEN,
          })}
        ></div>
      </div>
    );
  }
}

export default TrafficLightComp;