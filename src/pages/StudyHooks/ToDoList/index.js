import React, { useState } from 'react';
import ToDoForm from '../../../components/Hooks/ToDoForm'
import TodoList from '../../../components/Hooks/TodoList'
import { Container } from 'reactstrap';
import './style.scss'

function ToDoList(props) {
  const [todoList, setTodoList] = useState([
    { id: 1, title: 'đi ỉa' },
    { id: 2, title: 'đi ăn' },
    { id: 3, title: 'đi hên hò' },
    { id: 4, title: 'đi học' },
  ]);
  const handleTodoList = (todo) => {
    // setTodoList
    const index = todoList.findIndex(x => x.id === todo.id)
    if (index < 0) return
    const todoNeedRemove = [...todoList]
    todoNeedRemove.splice(index, 1)
    setTodoList(todoNeedRemove)
  }
  const handleAddList = (list) => {
    let todoNeedAdd = [...todoList]
    const randomIndex = Math.random() * 5
    let formAdd = {
      id: randomIndex,
      title: list
    }
    todoNeedAdd = [...todoNeedAdd, formAdd]
    setTodoList(todoNeedAdd)

  }
  return (
    <div>
      <Container>
        <h1>hook useState()</h1>
        <ul className="docs"> Là một hook cơ bản.
          <li>1/ Giúp mình có thể dùng state trong functional component.</li>
          <li>2/ Input: initialState (giá trị hoặc function)</li>
          <li>3/ Output: một mảng có 2 phần tử tương ứng cho <b>state</b> và <b>setState</b></li>
          <li>4/ Cách dùng: <b>const [name, setName] = useState('default');</b></li>
          <li><a href="https://drive.google.com/file/d/1Whnf_XkXGnawOU9Cobw9rzZCHwmbE6of/view" target="_blank"> docs</a></li>
        </ul>
        <h2>Demo</h2>
        <ToDoForm onSubmit={handleAddList} />
        <p><strong>Click vào task để xóa</strong></p>
        <TodoList todos={todoList} onTodoClick={handleTodoList} />
        <br />
      </Container>
    </div>
  );
}

export default ToDoList;