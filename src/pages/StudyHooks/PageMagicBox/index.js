import React from 'react';
import MagicBox from '../../../components/Hooks/MagicBox'
import { Container } from 'reactstrap';

function PageMagicBox() {
  return (
    <Container>
      <h1>Custom Hooks</h1>
      <ul className="docs">
        <li>1/ Là 1 hooks do mình tự định nghĩa ra và là 1 function đặc biệt</li>
        <li>2/ Có thể sử dụng các hooks khaca1 như useState, useEffect.... hoặc 1 custom hook khác</li>
        <li>3/ format file name: use[HookName], VDL useMagicColor....</li>
        <li>Output trả ra tùy mình quyết định (ra object, array....)</li>
        <li><a href="https://drive.google.com/file/d/1p6YT6wF90VxvlSO7YTA7ojrMcrP-H5r9/view" target="_blank"> docs</a></li>
      </ul>
      <h2>Demo (1 custom hook color change every 1 second, does not match the previous color)</h2>
      <MagicBox />
    </Container>
  );
}

export default PageMagicBox;