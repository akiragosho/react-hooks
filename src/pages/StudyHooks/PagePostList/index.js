import React, { useState, useEffect } from 'react';
import PostList from '../../../components/Hooks/PostList'
import Pagination from '../../../components/Common/Pagination'
import PostFiltersForm from '../../../components/Hooks/PostFiltersForm'
import './style.scss'
import queryString from 'query-string';
import axios from 'axios';
import { Container } from 'reactstrap';

function PagePostList(props) {
  const [postList, setPostList] = useState([]);
  const [pagination, setPagination] = useState({
    _page: 1,
    _limit: 10,
    _totalRows: 1
  });
  const [filter, setFilter] = useState({
    _limit: 10,
    _page: 1,
  })

  useEffect(() => {
    async function fetchPostList() {
      const paramsPage = queryString.stringify(filter)
      const res = await axios.get(`http://js-post-api.herokuapp.com/api/posts?${paramsPage}`)
      if (res.status === 200) {
        const listPost = res.data.data
        const pagination = res.data.pagination
        setPostList(listPost)
        setPagination(pagination);
      }
    }
    fetchPostList();
  }, [filter])  // giống componentDidMount()
  function handlePageChange(newPage) {
    setFilter({
      ...filter,
      _page: newPage
    })
  }
  function handleFiltersChange(newFilter) {
    setFilter({
      ...filter,
      _page: 1,
      title_like: newFilter.searchTerm
    })
  }
  return (
    <Container className="page-list">
      <h1>useEffect</h1>
      <ul className="docs"> đc thực thi sau mỡi lần render
        <li>1/ Nếu ko truyền dependencies thì luôn luôn chạy</li>
        <li>2/ nếu truyền [] thì chỉ chạy đúng 1 lần</li>
        <li>3/ Nếu truyền filter, chỉ chạy lần đầu, những lần sau có chạy hay ko phụ thuộc vào filter có thay đổi hay ko</li>
        <li><a href="https://drive.google.com/file/d/1CXxCJsabcG3IkSG1PQaWNVv12ePYICA1/view" target="_blank"> docs</a></li>
      </ul>
      <h2>Demo</h2>
      <p>Search: </p>
      <PostFiltersForm onSubmit={handleFiltersChange} />
      <br />
      <p>List from API: </p>
      <PostList posts={postList} />
      <p>Pagination (prev, next)</p>
      <Pagination pagination={pagination} onPageChange={handlePageChange} />
    </Container>
  );
}

export default PagePostList;