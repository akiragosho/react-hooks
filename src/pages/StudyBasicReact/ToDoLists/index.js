import React, { Component } from "react";
import ToDoItem from "./../../../components/Basics/ToDoItem";
import "./style.scss";

class ToDoLists extends Component {
  constructor() {
    super();
    this.state = {
      todoList: [{ id: 0, name: "di cho", status: true }],
    };
  }
  changeStatus = (item) => {
    const status = item.status;
    const { todoList } = this.state;
    const index = todoList.indexOf(item);
    this.setState({
      todoList: [
        ...todoList.slice(0, index),
        {
          ...item,
          status: !status,
        },
        ...todoList.slice(index + 1),
      ],
    });
  };
  addTask = (event) => {
    const { todoList } = this.state;
    let text = event.target.value;
    let num = 1;
    text = text.trim();
    if (event.keyCode === 13) {
      //Enter key
      if (text === "") return;
      num += 1;
      this.setState({
        todoList: [
          {
            id: Math.random(),
            name: text,
            status: false,
          },
          ...todoList,
        ],
      });
      event.target.value = "";
      console.log("text ", text);
    }
  };
  removeTask = (item) => {
    const { todoList } = this.state;
    console.log(item);
    this.setState({
      todoList: todoList.filter((task) => task.id !== item.id),
    });
  };
  render() {
    const { todoList } = this.state;
    return (
      <div className="ToDoLists">
        <h1>TODOS</h1>
        <div className="list-todo">
          <input
            type="text"
            placeholder="nhap di nào......"
            onKeyUp={this.addTask}
          ></input>
          <ul>
            {todoList.length > 0 &&
              todoList.map((item, index) => (
                <ToDoItem
                  item={item}
                  key={index}
                  onClick={() => this.changeStatus(item)}
                  removeTask={() => this.removeTask(item)}
                />
              ))}
            {todoList.length === 0 && <li>Nothing here!!!</li>}
          </ul>
        </div>
      </div>
    );
  }
}
export default ToDoLists;
