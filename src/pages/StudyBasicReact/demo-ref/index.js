import React, { Component } from 'react';
import classNames from "classnames";


class DemoRef extends Component {
  constructor(props){
    super();
    this.inputElement = React.createRef();
   
  }
  componentDidMount(){
    this.inputElement.current.focus()
  }
  render() { 
    return ( 
      <div>
        <h1 className="mb-3">focus vào input ngay khi load page</h1>
        <input type="text" ref={this.inputElement} />
      </div>
     );
  }
}
 
export default DemoRef;
