import React, { Component } from "react";
import TrafficLightComp from "../../../components/Basics/TrafficLight";
import "./style.scss";
const RED = 0;
const ORAGE = 1;
const GREEN = 2;
class TrafficLight extends Component {
  constructor() {
    super();
    this.state = {
      currentColor: RED,
      test: "",
      isRun: true,
    };
    this.state.test = this.changeColor();
  }
  changeColor = () => {
    return setInterval(() => {
      // console.log('this.currentColor ', this.currentColor);
      this.setState({
        currentColor: this.getNextColor(this.state.currentColor),
      });
    }, 1000);
  }
  getNextColor(color) {
    switch (color) {
      case RED:
        return ORAGE;
      case ORAGE:
        return GREEN;
      default:
        return RED;
    }
  }
  stopRun = () =>{
    const { isRun } = this.state;

    this.setState({
      isRun: !isRun
    })

    !isRun ? this.state.test = this.changeColor() : clearInterval(this.state.test) ;
  }
  render() {
    const { currentColor, isRun} = this.state;
    return (
      <div>
        <TrafficLightComp currentColor={currentColor} />
        <button onClick={this.stopRun}>{
          isRun ? <span>STOP</span> : <span>RUN</span> 
        }</button>
      </div>
    );
  }
}

export default TrafficLight;
