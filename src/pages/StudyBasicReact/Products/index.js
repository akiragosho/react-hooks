import React, { Component } from 'react';
import axios from 'axios'
import {
  Container, Row, Col, Card, CardImg, CardText, CardBody,
  CardTitle, CardSubtitle, Button, Input
} from 'reactstrap';
import './style.scss'

class Products extends Component {
  constructor(props) {
    super(props);
    this.state = {
      products: [],
      amount: 0,
    }
  }
  componentDidMount = () => {
    axios.get('https://tht5z.sse.codesandbox.io/products').then(res => {
      console.log(res);
      this.setState({
        products: res.data
      })
    })
  }
  addToCart = (product, amount) => {
    console.log('product ', product, amount);
  }
  render() {
    const { products, amount } = this.state
    return (
      <div>
        <h1>Nếu thấy không có gì thì vào api <a href="https://tht5z.sse.codesandbox.io/" target="_blank">đây</a> F5 lại cái nhé :))</h1>
        <Container className="mt-5">
          <Row>
            {
              products.map((product, index) => (
                <Col className="mb-3" xs="6" sm="3" key={index}>
                  <Card>
                    <CardImg className="item-image" top width="100%" src={product.image} alt={product.product_desc} />
                    <CardBody>
                      <CardTitle tag="h5">{product.product_name}</CardTitle>
                      <CardText tag="h6" className="mb-1 text-muted original-price">{product.price}</CardText>
                      <CardSubtitle tag="h6" className="mb-2 sale-price">{product.sale_price}</CardSubtitle>
                      <CardText className="amount">SL: {product.amount}</CardText>
                      <CardText className="desc">{product.product_desc}</CardText>
                      <Row className="mb-3">
                        <Col xs="12" sm="5" className="pt-2">Aquality: </Col>
                        <Col xs="12" sm="7"><Input type="number" xs="6" sm="3" min="1" defaultValue={amount}></Input></Col>
                      </Row>
                      <Button onClick={() => this.addToCart(product.id, amount)}>Add to cart</Button>
                    </CardBody>
                  </Card>
                </Col>
              ))
            }
          </Row>
        </Container>
      </div>
    );
  }
}

export default Products;
