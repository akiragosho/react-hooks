import React, { Component } from "react";
import ChildrenComp from '../../../components/Basics/ChildrenComp'
class ChildrenLesson extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isOpen:true
    }
  }
  onClick = () =>{
    const { isOpen } = this.state
    this.setState({
      isOpen: !isOpen
    })
    console.log('isOpen ', isOpen);
  }
  render() {
    const { isOpen } = this.state
    return ( 
      <div>
        <h1 onClick={this.onClick}>title</h1>
        {
          isOpen && <ChildrenComp>
          ben trong nay la children
        </ChildrenComp>
        }
      </div>
     );
  }
}

export default ChildrenLesson;
